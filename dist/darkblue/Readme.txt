Frio Theme for Freshdesk Support Center
===

About: http://www.breezythemes.com/products/freshdesk-theme-frio 
Demo Support Center: https://frio.breezythemes.com
Installation Guide: https://breezythemes.freshdesk.com/support/solutions/articles/2100022486-how-to-install-a-theme-
Refund Policy: https://checkout.shopify.com/16702687/policies/30377931.html

© Breezy Themes