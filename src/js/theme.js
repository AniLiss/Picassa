(function() {
  function clipper(min, max){
    return function(o){
      if(o > max) return max;
      if(o < min) return min;
      return o;
    };
  }

  var opacityClipper = clipper(0, 1);
  var $window      = jQuery(window);
  var $heroUnit    = jQuery('[data-hero-unit]');
  var $topbar      = jQuery('[data-topbar]');
  var topbarHeight = parseInt($topbar.height());
  var $scrollToTop = jQuery('[data-scroll-to-top]');
  var activeClass = 'is-active';
  var $heroUnitBg;
  var $searchBox;

  if ($heroUnit.length) {
    $heroUnitBg  = $heroUnit.find('[data-hero-bg]');
    $searchBox   = $heroUnit.find('[data-search-box]');
  }

  $scrollToTop.click(function(){
    jQuery('html, body').animate({ scrollTop: 0}, 1000);
    return false;
  });

  var bindEffects = function() {
    var scrolled = $window.scrollTop();
    if (scrolled > topbarHeight) {
      $scrollToTop.addClass(activeClass);
    } else {
      $scrollToTop.removeClass(activeClass);
    }

    if ($heroUnit.length) {
      $heroUnitBg.css({
        '-moz-transform': 'translate3d(0px,' + scrolled / -3 + 'px' +  ', 0px)',
        '-webkit-transform': 'translate3d(0px,' + scrolled / -3 + 'px' +  ', 0px)',
        'transform': 'translate3d(0px,' + scrolled / -3 + 'px' +  ', 0px)'
      });

      $searchBox.css({
        'opacity': opacityClipper(1 - opacityClipper(scrolled * 0.003))
      });
    }
  };

  if (!(/Android|iPhone|iPad|iPod|BlackBerry|Windows Phone/i)
    .test(navigator.userAgent || navigator.vendor || window.opera)) {
    if (Modernizr.csstransforms3d) {
      $window.on('scroll.theme', bindEffects);
    }
  }

  jQuery('[data-toggle-menu]').click(function(){
    jQuery(this).toggleClass('is-active');
    jQuery('[data-menu]').toggle();
  });
})();